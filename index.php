<?php
	session_start();
	$max = 20;
?>
<!DOCTYPE html>
<html lang="de">
	<head>
  	<title>Sarahs Mathematik Aufgaben</title>
	  <meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
		<style>
			.container {
				margin: 0;
				position: absolute;
				top: 50%;
				left: 50%;
				transform: translate(-50%, -50%);
			}

			.alert {
				position: relative;
			}

			.alert .label {
				position: absolute;
				right: 20px;
				top: -20px;
			}
		</style>
	</head>
	<body>
		<div class="container text-center">
			<div class="row content">
				<div class="col-xs-12">
					<h1>Mathematik für Sarah</h1>
				</div>
			</div>
<?php
	$tasks = intval($_SESSION['tasks']);
	$correcttasks = intval($_SESSION['correcttasks']);
	if (!empty($_POST)) {
		$tasks++;
		$_SESSION['tasks'] = $tasks;
		$operator = $_POST['operator'];
		$first = intval($_POST['first']);
		$second = intval($_POST['second']);
		switch ($operator) {
			case '+':
				$correct = $first + $second;
				break;
			case '-':
				$correct = $first - $second;
				break;
			case '*':
				$correct = $first * $second;
				break;
		}
?>
			<div class="row content">
				<div class="col-xs-12 h1">
<?php
		if ($correct == $_POST['solution']) {
			$correcttasks++;
			$_SESSION['correcttasks'] = $correcttasks;
?>
				
					<div class="alert alert-success" role="alert">
						Richtig
						<span class="label label-success"><?=$correcttasks ?> von <?=$tasks ?> richtig</span>	
					</div>
				
<?php
		}
		else {
?>
					<div class="alert alert-danger" role="alert">
						Falsch - die richtige Antwort ist: <?=$correct ?>
						<span class="label label-success"><?=$correcttasks ?> von <?=$tasks ?> richtig</span>	
					</div>
<?php
			$explanation = '';
			if ($operator === '*') {
				if ($second % 10 !== 0) {
					$factorOne = $second;
					$factorTwo = $first;
				}
				else if ($first % 10 !== 0) {
					$factorOne = $first;
					$factorTwo = $second;
				}
				if (!empty($factorOne) && !empty($factorTwo)) {
					$modulo = $factorOne % 10;
					$resultOne = 10 * $factorTwo;
					$explanation .= '<div class="row content">
														<div class="col-xs-12 h2">' . $first . ' * '. $second .' = <strong>?</strong></div>
													</div>';
					$explanation .= '<div class="row content">
														<div class="col-xs-12 h2">' . $factorTwo . ' * 10 = <span class="label label-primary">' .$resultOne . '</span></div>
													</div>';
					$resultTwo = $modulo * $factorTwo;
					$explanation .= '<div class="row content">
														<div class="col-xs-12 h2">' . $factorTwo . ' * ' . $modulo .' = <span class="label label-info">' .$resultTwo . '</span></div>
													 </div>';
					$resultThree = $resultOne + $resultTwo;
					$explanation .= '<div class="row content">
														<div class="col-xs-12 h2">
															<span class="label label-primary">' .$resultOne . '</span> + <span class="label label-info">' .$resultTwo . '</span> = <strong>' .$resultThree . '</strong>
														</div>
													 </div>';
				}
			}
			if (!empty($explanation)) {
?>
				<div class="row content">
					<div class="col-xs-12">
						<h2>Lösungsweg:</h2>
					</div>
				</div>
				<?= $explanation ?>
<?php
			}
		}
?>
				</div>
			</div>
			<div class="row content">
				<div class="col-xs-12"><a href="/" class="btn btn-primary btn-lg">Nächste Aufgabe</a></div>
			</div>
<?php
	}
	else {
		$operators = ['-', '+', '*'];
		$operator = $operators[array_rand($operators)];
		$first = rand(1, $max);
		$second = rand(1, $max);
?>
			<form method="post">
					<input type="hidden" name="first" value="<?=$first ?>"/>
					<input type="hidden" name="operator" value="<?=$operator ?>"/>
					<input type="hidden" name="second" value="<?=$second ?>"/>
  	  	  <div class="row content">
						<div class="col-xs-3 h1"><?=$first ?></div>
						<div class="col-xs-3 h1"><?=$operator ?></div>
						<div class="col-xs-3 h1"><?=$second ?></div>
						<div class="col-xs-3 h1">=</div>
						<div class="col-xs-12 h1"><input class="form-control" type="number" name="solution"/></div>
  				</div>
					<div class="row content">
						<div class="col-xs-12"><input type="submit" class="btn btn-primary btn-lg" value="Lösen"/></div>
					</div>
			</form>
<?php
	}
?>
		</div>
	</body>
</html>